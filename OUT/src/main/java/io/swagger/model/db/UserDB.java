package io.swagger.model.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.UUID;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDB {
    private UUID id;

    private String name;

    private String surname;

    private Integer age;

    private String personalId;

    private String citizenship;

    private String email;
}
