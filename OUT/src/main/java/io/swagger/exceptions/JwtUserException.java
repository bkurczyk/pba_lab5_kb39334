package io.swagger.exceptions;

public class JwtUserException extends RuntimeException {
    public JwtUserException() {
        super();
    }

    public JwtUserException(String message) {
        super(message);
    }

    public JwtUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public JwtUserException(Throwable cause) {
        super(cause);
    }

    protected JwtUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
